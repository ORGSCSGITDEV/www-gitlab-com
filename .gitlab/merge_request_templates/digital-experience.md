**Reviewer Checklist:**
- [ ] These changes meet a specific OKR or item in our Quarterly Plan.
- [ ] These changes work as intended.
- [ ] These changes work on both Safari and Chrome.
- [ ] These changes have been reviewed for Visual Quality Assurance and Functional Quality Assurance on Mobile, Desktop, and Tablet.
- [ ] These changes have been documented as expected.
